require 'sidekiq/web'

Rails.application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'

  resources :uploads, only: [:new, :create, :index]
  resources :products, only: :index

  root 'uploads#index'
end
