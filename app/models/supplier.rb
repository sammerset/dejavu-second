class Supplier < ActiveRecord::Base
  has_many :products, foreign_key: :uniq_ident, primary_key: :supplier_uniq_ident

  validates :uniq_ident, uniqueness: true, presence: true, length: { in: 2..255 }
  validates :name, presence: true, length: { in: 2..255 }
end
