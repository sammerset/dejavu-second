class Upload < ActiveRecord::Base
  paginates_per 20

  DATA_TYPES = [:products, :suppliers]
  DATA_TYPES_COLUMNS_SIZE = { products: 8, suppliers: 1 }
  DATA_DELIMITER = '\xa6'

  has_attached_file :file
  validates_attachment :file, presence: true,
    content_type: { content_type: "text/csv" },
    size: { in: 0..100.megabytes }
  validates :data_type, inclusion: { in: ['products', 'suppliers'] }
  validate :partial_file_validation, on: :create

  after_commit :start_background_job, on: :create

  state_machine initial: :new do
    before_transition   on: :start,            do: 'upload_to_temp_table!'
    before_transition   on: :insert,           do: 'insert_data!'
    after_transition    on: [:finish, :broke],  do: 'clean_temp_data!'
    after_failure       on: :all,              do: 'broke!'

    event :start do
      transition :new => :uploaded
    end

    event :insert do
      transition :uploaded => :inserted
    end

    event :finish do
      transition :inserted => :finished
    end

    event :broke do
      transition [:new, :uploaded, :inserted, :finished] => :broken
    end
  end

  private
  
  def partial_file_validation
    @test_file = Tempfile.new('test_temp', '/tmp')
    begin
      open(file.queued_for_write[:original].path).first(10).each do |line|
        raise 'broken columns number' if line.scan(/¦/).size != 
                                           DATA_TYPES_COLUMNS_SIZE[:"#{data_type}"]
        @test_file.puts(line)
      end
      FileUtils.chmod(0755, @test_file)
      upload_to_temp_table!
      create_test_table!
      insert_data!(false)
      clean_temp_data!
    rescue => e
      errors.add(:broken_file, "Can't upload file because of reason: #{e}")
    ensure
      @test_file.close
      @test_file.unlink
    end
    @test_file = nil
  end

  def upload_to_temp_table!
    clean_temp_data!
    execute_with_detect_failures(send("#{data_type}_create_temp_table_query"))
    execute_with_detect_failures(copy_query)
  end

  def insert_data!(test=false)
    execute_with_detect_failures(send("insert_#{data_type}_parsed_data", test))
  end

  def clean_temp_data!
    execute_with_detect_failures(%Q[ DROP TABLE IF EXISTS temp_upload_table_#{id}; ])
    execute_with_detect_failures(%Q[ DROP TABLE IF EXISTS temp_test_#{data_type}; ])
  end

  def execute_with_detect_failures(query)
    begin
      ActiveRecord::Base.connection.execute(query)
    rescue => e
      parse_errors = "Upload to temp table error: #{e}"
      raise e if @test_file
    end
    parse_errors.blank?
  end

  def products_create_temp_table_query
    %Q[ CREATE TABLE temp_upload_table_#{id} 
          ( id SERIAL,
            SKU varchar(255) NOT NULL, SUPPLIER_UNIQ_IDENT char(255) NOT NULL,
            FIELD_0 char(255), FIELD_1 char(255), FIELD_2 char(255), 
            FIELD_3 char(255), FIELD_4 char(255), FIELD_5 char(255),
            PRICE NUMERIC(8,2) ); ]
  end

  def suppliers_create_temp_table_query
    %Q[ CREATE TABLE temp_upload_table_#{id} 
          ( id SERIAL, 
            UNIQ_IDENT char(255) NOT NULL, NAME char(255) NOT NULL ); ]
  end

  def copy_query
    uploaded_file = @test_file || file
    %Q[ COPY temp_upload_table_#{id} (#{data_columns.join(',')}) FROM '#{uploaded_file.path}' CSV DELIMITER AS E'#{DATA_DELIMITER}' ENCODING 'UTF8'; ]
  end

  def data_columns
    case data_type
    when 'products'
      ['sku', 'supplier_uniq_ident', 'field_0',
       'field_1', 'field_2', 'field_3', 'field_4',
       'field_5', 'price']
    else
      ['uniq_ident', 'name']
    end
  end

  def insert_products_parsed_data(test=false)
    %Q[ INSERT INTO #{test ? 'temp_test_': ''}products ( #{data_columns.join(',')} )
        SELECT #{data_columns.map{|c| "ps_1.#{c}"}.join(',')}
        FROM temp_upload_table_#{id} as ps_1
          LEFT JOIN temp_upload_table_#{id} as ps_2
          ON (ps_1.sku = ps_2.sku AND ps_1.id < ps_2.id)
        WHERE ps_2.id IS NULL
        ON CONFLICT (sku) DO UPDATE SET
          supplier_uniq_ident = excluded.supplier_uniq_ident,
          #{(0..5).map{|i| "field_#{i} = excluded.field_#{i}" }.join(', ')},
          price = excluded.price ]
  end

  def insert_suppliers_parsed_data(test=false)
    %Q[ INSERT INTO #{test ? 'temp_test_': ''}suppliers ( #{data_columns.join(',')} )
        SELECT #{data_columns.map{|c| "ps_1.#{c}"}.join(',')}
        FROM temp_upload_table_#{id} as ps_1
          LEFT JOIN temp_upload_table_#{id} as ps_2
          ON (ps_1.uniq_ident = ps_2.uniq_ident AND ps_1.id < ps_2.id)
        WHERE ps_2.id IS NULL
        ON CONFLICT (uniq_ident) DO UPDATE SET
          name = excluded.name ]
  end

  def create_test_table!
    %Q[ CREATE TABLE temp_test_#{data_type} AS SELECT * FROM #{data_type} ]
  end

  def start_background_job
    UploadBatchParserCreatorWorker.perform_async(id)
  end
end
