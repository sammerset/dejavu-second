class Product < ActiveRecord::Base
  paginates_per 100

  belongs_to :supplier, foreign_key: :supplier_uniq_ident, primary_key: :uniq_ident

  validates :sku, presence: true, uniqueness: true, length: { in: 2..255 }
  validates :supplier_uniq_ident, presence: true, length: { in: 2..255 }
  validates *((0..5).map{|i| "field_#{i}"}), length: { maximum: 255 }
  validates :price, numericality: true, length: { maximum: 9 }, 
               format: { with: /\A\d+(\.{1}\d{1,2})?\Z/ }
end
