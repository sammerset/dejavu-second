class UploadsController < ApplicationController
  def create
    @upload = Upload.new(upload_params[:upload])
    if @upload.save
      flash[:success] = 'File was uploaded and will be processed!'
      redirect_to root_path
    else
      flash[:error] = 'Its few failures when uploading file.'
      render :new
    end
  end

  def index
    @uploads = Upload.order(:id).page(params[:page])
  end

  def new
    @upload = Upload.new
  end

  private

  def upload_params
    params.permit(upload: [:file, :data_type])
  end
end
