class UploadBatchParserCreatorWorker
  include Sidekiq::Worker

  def perform(upload_id)
    upload = Upload.find(upload_id)
    
    upload.start!
    upload.insert!
    upload.finish!
  end
end
