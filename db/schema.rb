# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160801222203) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "products", force: true do |t|
    t.string   "field_0"
    t.string   "field_1"
    t.string   "field_2"
    t.string   "field_3"
    t.string   "field_4"
    t.string   "field_5"
    t.decimal  "price",               precision: 8, scale: 2
    t.string   "supplier_uniq_ident"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "sku"
  end

  add_index "products", ["sku"], name: "index_products_on_sku", unique: true, using: :btree

  create_table "reservations", force: true do |t|
    t.integer  "table_id"
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "reservations", ["end_time"], name: "index_reservations_on_end_time", using: :btree
  add_index "reservations", ["start_time"], name: "index_reservations_on_start_time", using: :btree

  create_table "suppliers", force: true do |t|
    t.string   "uniq_ident"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
  end

  add_index "suppliers", ["uniq_ident"], name: "index_suppliers_on_uniq_ident", unique: true, using: :btree

  create_table "uploads", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.string   "state"
    t.string   "data_type"
    t.text     "parse_errors"
  end

end
