class CreateSuppliers < ActiveRecord::Migration
  def change
    create_table :suppliers do |t|
      t.string :uniq_ident

      t.timestamps
    end
  end
end
