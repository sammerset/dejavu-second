class AddColumnsToUploads < ActiveRecord::Migration
  def change
    add_column :uploads, :state, :string
    add_column :uploads, :data_type, :string
    add_column :uploads, :parse_errors, :text
  end
end
