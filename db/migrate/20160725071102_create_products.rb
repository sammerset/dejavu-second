class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :field_0
      t.string :field_1
      t.string :field_2
      t.string :field_3
      t.string :field_4
      t.string :field_5
      t.decimal :price, precision: 8, scale: 2
      t.string :supplier_uniq_ident

      t.timestamps
    end
  end
end
