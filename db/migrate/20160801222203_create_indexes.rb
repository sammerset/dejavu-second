class CreateIndexes < ActiveRecord::Migration
  def change
    add_index :products, :sku, unique: true
    add_index :suppliers, :uniq_ident, unique: true
  end
end
