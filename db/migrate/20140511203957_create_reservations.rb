class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.integer :table_id
      t.datetime :start_time
      t.datetime :end_time

      t.timestamps
    end
    add_index :reservations, :start_time
    add_index :reservations, :end_time
  end
end
