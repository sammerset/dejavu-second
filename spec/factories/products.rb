FactoryGirl.define do
  factory :product do
    field_0 "MyString1"
    field_1 "MyString2"
    field_2 "MyString3"
    field_3 "MyString4"
    field_4 "MyString5"
    field_5 "MyString6"
    sku     "MyString7"
    price   9.99
    supplier_uniq_ident "MyString8"
  end
end
