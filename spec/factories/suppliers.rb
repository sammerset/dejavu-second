FactoryGirl.define do
  factory :supplier do
    uniq_ident "MyString"
    name       "SupplierName"
  end
end
