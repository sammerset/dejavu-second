include ActionDispatch::TestProcess

FactoryGirl.define do
  factory :upload do
    name "MyString"
    data_type "products"
    file { fixture_file_upload "#{Rails.root}/spec/fixtures/files/sku.csv", 'text/csv' }

    factory :products_upload do
      data_type "products"
      file { fixture_file_upload "#{Rails.root}/spec/fixtures/files/sku.csv", 'text/csv' }
      factory :repeated_products_upload do
        file { fixture_file_upload "#{Rails.root}/spec/fixtures/files/repeated_sku.csv", 'text/csv' }
      end
      factory :invalid_products_upload do
        to_create {|instance| instance.save(validate: false) }
        file { fixture_file_upload "#{Rails.root}/spec/fixtures/files/invalid_sku.csv", 'text/csv' }
      end
    end

    factory :suppliers_upload do
      data_type "suppliers"
      file { fixture_file_upload "#{Rails.root}/spec/fixtures/files/suppliers.csv", 'text/csv' }
      factory :repeated_suppliers_upload do
        file { fixture_file_upload "#{Rails.root}/spec/fixtures/files/repeated_suppliers.csv", 'text/csv' }
      end
      factory :invalid_suppliers_upload do
        to_create {|instance| instance.save(validate: false) }
        file { fixture_file_upload "#{Rails.root}/spec/fixtures/files/invalid_suppliers.csv", 'text/csv' }
      end
    end
  end
end
