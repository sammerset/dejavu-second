require 'spec_helper'

RSpec.describe Product, type: :model do
  it { should validate_presence_of(:sku).with_message('can\'t be blank') }
  it { should validate_presence_of(:supplier_uniq_ident).with_message('can\'t be blank') }

  it { should allow_value(123456.78).for(:price) }
  it { should allow_value(1234567.0).for(:price) }

  it { should validate_numericality_of(:price) }
  it { should_not allow_value('abcd').for(:price) }
  it { should_not allow_value('1234567.89').for(:price) }
  it { should_not allow_value('123456789.0').for(:price) }
  it { should_not allow_value('12345.678').for(:price) }

  it { should validate_length_of(:sku).is_at_least(2)
                       .with_message(/is too short \(minimum is 2 characters\)/) }
  it { should validate_length_of(:sku).is_at_most(255)
                       .with_message(/is too long \(maximum is 255 characters\)/) }

  it { should validate_length_of(:supplier_uniq_ident).is_at_least(2)
                       .with_message(/is too short \(minimum is 2 characters\)/) }
  it { should validate_length_of(:supplier_uniq_ident).is_at_most(255)
                       .with_message(/is too long \(maximum is 255 characters\)/) }

  it { should validate_length_of(:supplier_uniq_ident).is_at_least(2)
                       .with_message(/is too short \(minimum is 2 characters\)/) }
  it { should validate_length_of(:supplier_uniq_ident).is_at_most(255)
                       .with_message(/is too long \(maximum is 255 characters\)/) }

  (0..5).each do |i|
    it { should allow_value(nil).for(:"field_#{i}") }
    it { should allow_value("").for(:"field_#{i}") }
    it { should validate_length_of(:"field_#{i}").is_at_most(255)
                       .with_message(/is too long \(maximum is 255 characters\)/) }
  end

  it { should belong_to(:supplier).with_foreign_key(:supplier_uniq_ident)
                                  .with_primary_key(:uniq_ident) }
end
