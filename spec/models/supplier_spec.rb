require 'spec_helper'

RSpec.describe Supplier, type: :model do
  it { should validate_presence_of(:name).with_message('can\'t be blank') }
  it { should validate_presence_of(:uniq_ident).with_message('can\'t be blank') }

  it { should validate_length_of(:name).is_at_least(2)
                       .with_message(/is too short \(minimum is 2 characters\)/) }
  it { should validate_length_of(:name).is_at_most(255)
                       .with_message(/is too long \(maximum is 255 characters\)/) }
  it { should validate_length_of(:uniq_ident).is_at_least(2)
                       .with_message(/is too short \(minimum is 2 characters\)/) }
  it { should validate_length_of(:uniq_ident).is_at_most(255)
                       .with_message(/is too long \(maximum is 255 characters\)/) }

  it { should have_many(:products).with_foreign_key(:uniq_ident)
                                         .with_primary_key(:supplier_uniq_ident) }
end
