require 'spec_helper'
require "paperclip/matchers"

RSpec.describe Upload, type: :model do
  it { should validate_inclusion_of(:data_type).in_array(['products', 'suppliers']) }

  describe 'upload validation' do
    it 'products partial_file_validation' do
      upload = build(:products_upload)
      upload.send(:partial_file_validation)
      expect(upload.errors).to be_empty
    end

    it 'invalid products partial_file_validation' do
      upload = build(:invalid_products_upload)
      upload.send(:partial_file_validation)
      expect(upload.errors[:broken_file].first).to include("Can't upload file because of reason")
    end

    it 'suppliers partial_file_validation' do
      upload = build(:suppliers_upload)
      upload.send(:partial_file_validation)
      expect(upload.errors).to be_empty
    end

    it 'invalid suppliers partial_file_validation' do
      upload = build(:invalid_suppliers_upload)
      upload.send(:partial_file_validation)
      expect(upload.errors[:broken_file].first).to include("Can't upload file because of reason")
    end
  end

  describe 'state machine' do
    subject { create(:products_upload) }

    it 'initial state should be new' do
      should be_new
    end

    it '#start! changes level to uploaded from new' do
      subject.state = 'new'
      expect { subject.start! }.to change(subject, :state).from('new').to('uploaded')
    end

    it '#insert! changes level to inserted from uploaded' do
      subject.start!
      subject.state = 'uploaded'
      expect { subject.insert! }.to change(subject, :state).from('uploaded').to('inserted')
    end

    it '#finish! changes level to finished from inserted' do
      subject.start!
      subject.insert!
      subject.state = 'inserted'
      expect { subject.finish! }.to change(subject, :state).from('inserted').to('finished')
    end

    it '#broke! changes level to broken from new' do
      subject.state = 'new'
      expect { subject.broke! }.to change(subject, :state).from('new').to('broken')
    end

    it '#broke! changes level to broken from uploaded' do
      subject.state = 'uploaded'
      expect { subject.broke! }.to change(subject, :state).from('uploaded').to('broken')
    end

    it '#broke! changes level to broken from inserted' do
      subject.state = 'inserted'
      expect { subject.broke! }.to change(subject, :state).from('inserted').to('broken')
    end

    it '#broke! changes level to broken from finished' do
      subject.state = 'finished'
      expect { subject.broke! }.to change(subject, :state).from('finished').to('broken')
    end
  end
  
  describe 'worker' do
    it 'products perform' do
      upload = create(:products_upload)
      UploadBatchParserCreatorWorker.new.perform(upload.id)

      expect(Product.count).to equal(5)
      expect(Supplier.count).to be_zero
    end

    it 'suppliers perform' do
      upload = create(:suppliers_upload)
      UploadBatchParserCreatorWorker.new.perform(upload.id)

      expect(Supplier.count).to equal(5)
      expect(Product.count).to be_zero
    end

    it 'update products perform' do
      upload = create(:repeated_products_upload)
      UploadBatchParserCreatorWorker.new.perform(upload.id)

      expect(Product.count).to equal(5)
      expect(Supplier.count).to be_zero
    end

    it 'update suppliers perform' do
      upload = create(:repeated_suppliers_upload)
      UploadBatchParserCreatorWorker.new.perform(upload.id)

      expect(Supplier.count).to equal(5)
      expect(Product.count).to be_zero
    end
  end
end
