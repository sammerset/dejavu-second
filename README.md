# Test app "dejavu"
for run tests use """#: rake spec"""
for live testing you have to use rails console.

App use database use Latin1 locale.
Postgresql version > 9.5.
Don't forget to start sidekiq worker with
$sidekiq command.